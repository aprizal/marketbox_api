import { NestFactory } from '@nestjs/core';
import { AppModule } from './module';

async function mulai() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
mulai();
