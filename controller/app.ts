import {
    Controller,
    Get,
    Post,
    Body,
    Res,
    HttpStatus  
} from "@nestjs/common";
  import { AppService } from "../service";
  import { Response } from 'express';
  
  @Controller('uwu')
  export class AppController {
    constructor(private readonly appService: AppService) {}
  
    @Get()
    getHello(@Res() res: Response) {
       const main = this.appService.getHello();
        res.status(HttpStatus.OK).send(main)
    }
  
    @Post()
    create(@Res() res: Response, @Body() createCatDto) {
      const abc = this.appService.created(createCatDto)
      res.status(HttpStatus.CREATED).send(abc); 
    }
  }
  