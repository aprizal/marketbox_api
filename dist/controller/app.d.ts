import { AppService } from "../service";
import { Response } from 'express';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    getHello(res: Response): void;
    create(res: Response, createCatDto: any): void;
}
