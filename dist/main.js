"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const module_1 = require("./module");
async function mulai() {
    const app = await core_1.NestFactory.create(module_1.AppModule);
    await app.listen(3000);
}
mulai();
//# sourceMappingURL=main.js.map